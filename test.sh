#!/bin/bash

echo "------------------------------Test 1------------------------------"
echo "./wc /tmp/ee491f-wc-test-files/test1"
./wc /tmp/ee491f-wc-test-files/test1
echo "wc /tmp/ee491f-wc-test-files/test1"
wc /tmp/ee491f-wc-test-files/test1
echo "------------------------------Test 2------------------------------"
echo "./wc /tmp/ee491f-wc-test-files/test2"
./wc /tmp/ee491f-wc-test-files/test2
echo "wc /tmp/ee491f-wc-test-files/test2"
wc /tmp/ee491f-wc-test-files/test2
echo "------------------------------Test 3------------------------------"
echo "./wc /tmp/ee491f-wc-test-files/test3"
./wc /tmp/ee491f-wc-test-files/test3
echo "wc /tmp/ee491f-wc-test-files/test3"
wc /tmp/ee491f-wc-test-files/test3
echo "------------------------------Test 4------------------------------"
echo "./wc /tmp/ee491f-wc-test-files/test4"
./wc /tmp/ee491f-wc-test-files/test4
echo "wc /tmp/ee491f-wc-test-files/test4"
wc /tmp/ee491f-wc-test-files/test4
echo "------------------------------Test 5------------------------------"
echo "./wc /tmp/ee491f-wc-test-files/test5"
./wc /tmp/ee491f-wc-test-files/test5
echo "wc /tmp/ee491f-wc-test-files/test5"
wc /tmp/ee491f-wc-test-files/test5
echo "------------------------------Test 6------------------------------"
echo "./wc /tmp/ee491f-wc-test-files/test1 /tmp/ee491f-wc-test-files/test2"
./wc /tmp/ee491f-wc-test-files/test1 /tmp/ee491f-wc-test-files/test2
echo "wc /tmp/ee491f-wc-test-files/test1 /tmp/ee491f-wc-test-files/test2"
wc /tmp/ee491f-wc-test-files/test1 /tmp/ee491f-wc-test-files/test2
echo "------------------------------Test 7------------------------------"
echo "./wc -c /tmp/ee491f-wc-test-files/test1"
./wc -c /tmp/ee491f-wc-test-files/test1
echo "wc -c /tmp/ee491f-wc-test-files/test1"
wc -c /tmp/ee491f-wc-test-files/test1
echo "------------------------------Test 8------------------------------"
echo "./wc -l /tmp/ee491f-wc-test-files/test1"
./wc -l /tmp/ee491f-wc-test-files/test1
echo "wc -l /tmp/ee491f-wc-test-files/test1"
wc -l /tmp/ee491f-wc-test-files/test1
echo "------------------------------Test 9------------------------------"
echo "./wc -w /tmp/ee491f-wc-test-files/test1"
./wc -w /tmp/ee491f-wc-test-files/test1
echo "wc -w /tmp/ee491f-wc-test-files/test1"
wc -w /tmp/ee491f-wc-test-files/test1
echo "------------------------------Test 10------------------------------"
echo "./wc --bytes /tmp/ee491f-wc-test-files/test1"
./wc --bytes /tmp/ee491f-wc-test-files/test1
echo "wc --bytes /tmp/ee491f-wc-test-files/test1"
wc --bytes /tmp/ee491f-wc-test-files/test1
echo "------------------------------Test 11------------------------------"
echo "./wc --lines /tmp/ee491f-wc-test-files/test1"
./wc --lines /tmp/ee491f-wc-test-files/test1
echo "wc --lines /tmp/ee491f-wc-test-files/test1"
wc --lines /tmp/ee491f-wc-test-files/test1
echo "------------------------------Test 12------------------------------"
echo "./wc --words /tmp/ee491f-wc-test-files/test1"
./wc --words /tmp/ee491f-wc-test-files/test1
echo "wc --words /tmp/ee491f-wc-test-files/test1"
wc --words /tmp/ee491f-wc-test-files/test1
echo "------------------------------Test 13------------------------------"
echo "cat /tmp/ee491f-wc-test-files/test1 | ./wc"
cat /tmp/ee491f-wc-test-files/test1 | ./wc
echo "cat /tmp/ee491f-wc-test-files/test1 | wc"
cat /tmp/ee491f-wc-test-files/test1 | wc
