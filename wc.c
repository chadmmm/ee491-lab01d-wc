///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 491 - Software Reverse Engineering
/// Lab 01 - WordCount
///
/// @file wc.c
/// @version 2.0
///
/// @author chadMorita <chadmmm@hawaii.edu>
/// @brief  Lab 01 - WordCount - EE 491F - Spr 2021
/// @date   26_01_2021
/// @todo Implement UTF-16 support someday
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include "wc.h"


/*
 * Main function. Calls the functions for the wc program.
 *
 * @return int 0 on success 1 on error
 */
int main(int argc, char ** argv) {

   // Struct used to hold the line word and character counts.
   struct count_struct counts;

   // Used to keep track of the counts to print.
   bool print_bytes = false;
   bool print_lines = false;
   bool print_words = false;

   // FILE * to store the file if not reading from stdin
   FILE * file_pointer;

   // If there are no arguments, then read from standard in
   if (argc == 1) {
      count(stdin, &counts);
      printf("%d\t%d\t%d\t\n", counts.l_count, counts.w_count, counts.c_count);
      return EXIT_SUCCESS;
   }

   // Print the version.
   if (strncmp(argv[1], "--version", 9) == 0) {
      printf("wc 2.0\n");
      return EXIT_SUCCESS;
   }


   for(int i=1; i < argc; i++) {
      // Print bytes
      if ((strncmp(argv[i], "-c", 2) == 0) || (strncmp(argv[i], "--bytes", 7) == 0)) {
         print_bytes = true;
      // Print lines
      } else if ((strncmp(argv[i], "-l", 2) == 0) || (strncmp(argv[i], "--lines", 7) == 0)) {
         print_lines = true;

      // Print words
      } else if ((strncmp(argv[i], "-w", 2) == 0) || (strncmp(argv[i], "--words", 7) == 0)) {
         print_words = true;

      // Catch any invalid arguments.
      } else if ((strncmp(argv[i], "-", 1) == 0)) {
         fprintf(stderr, "wc: invalid option -- '%s'\n", argv[i]);
         return EXIT_FAILURE;

      // Must be a filename.
      } else {
  
         // Try to open file.
         file_pointer = fopen(argv[i], "r");
         if (file_pointer == NULL) {
            fprintf(stderr, "wc: Can't open [%s]\n", argv[i]);
            return EXIT_FAILURE;
         }

         // Process the file
         count(file_pointer, &counts);

         fclose(file_pointer);

         // If there are no options specified, print all the counts.
         if ((print_lines == false) && (print_words == false) && (print_bytes == false)) {
            printf("%d\t%d\t%d\t%s\n", counts.l_count, counts.w_count, counts.c_count, argv[i]);
         } else {
            if (print_lines == true) {
               printf("%d\t", counts.l_count);
            }
            if (print_words == true) {
               printf("%d\t", counts.w_count);
            }
            if (print_bytes == true) {
               printf("%d\t", counts.c_count);
            }
            printf("%s\n", argv[i]);
         }
      }

   }
   return EXIT_SUCCESS;
}

