///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 491 - Software Reverse Engineering
/// Lab 01 - WordCount
///
/// @file file_processor.c
/// @version 2.0
///
/// @author chadMorita <chadmmm@hawaii.edu>
/// @brief  Lab 01 - WordCount - EE 491F - Spr 2021
/// @date   26_01_2021
/// @todo Implement UTF-16 support someday
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include "wc.h"

/*
 * Determines whether a given character is considered a whitespace character.
 *
 * @param c The character to check
 * @return int 0 if the character is not a whitespace character and 1 if it is.
 */
int iswhitespace(char c) {
   if ((c == ' ') || (c == '\t') || (c == '\n') || (c == '\v') || (c == '\f') || (c == '\r')) {
      return 1;
   }
   return 0;
}

/* 
 * Function to count and print the number of lines, words and charachters in a
 * file.
 *
 * @param FILE * file_pointer The file to be counted
 * @return void
 */
void count(FILE * file_pointer, struct count_struct * count) {
   int c;
 
   // Enum to keep state of being in white space or not.
   enum State {IN_A_WORD = 0, IN_WHITESPACE = 1};

   // Initial state is in a word.
   enum State whitespace_state = IN_WHITESPACE;

   
   // Initialize all counts to 0.
   count->l_count = 0;
   count->w_count = 0;
   count->c_count = 0;

   while ((c = getc(file_pointer)) != EOF) {

      // Manage state between whitespace and in a word.
      if (iswhitespace(c) == 1){
         whitespace_state = IN_WHITESPACE; 
      } else {
         // If changing from in whitespace to in a word, increment the word count.
         if (whitespace_state == IN_WHITESPACE) {
            count->w_count++;
         }
         whitespace_state = IN_A_WORD;
      }

      // Keep track of the character count.
      count->c_count++;

      // Count the number of lines.
      if (c == '\n') {
         count->l_count++;
      }
   }

}

