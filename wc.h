///////////////////////////////////////////////////////////////////////////////
///// University of Hawaii, College of Engineering
///// EE 491 - Software Reverse Engineering
///// Lab 01 - WordCount
/////
///// @file wc.h
///// @version 2.0
/////
///// @author chadMorita <chadmmm@hawaii.edu>
///// @brief  Lab 01 - WordCount - EE 491F - Spr 2021
///// @date   26_01_2021
/////////////////////////////////////////////////////////////////////////////////

// Struct for holding counts and passing between functions
struct count_struct {
   int l_count;
   int w_count;
   int c_count;
};

/*
 * Function to count and print the number of lines, words and charachters in a
 * file.
 *
 * @param FILE * file_pointer The file to be counted
 * @return void
 */
void count(FILE *, struct count_struct *);

/*
 * Determines whether a given character is considered a whitespace character.
 *
 * @param c The character to check
 * @return int 0 if the character is not a whitespace character and 1 if it is.
 */
int iswhitespace(char);

