# Build a Word Counting program

CC     = gcc
CFLAGS = -c -g -Wall

TARGET = wc

all: $(TARGET)

wc: wc.o file_processor.o
	$(CC) -o wc wc.o file_processor.o

wc.o: wc.c
	$(CC) $(CFLAGS) wc.c

file_processor.o: file_processor.c 
	$(CC) $(CFLAGS) file_processor.c

test: wc
	./test.sh
clean:
	rm $(TARGET) *.o

